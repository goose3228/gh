require "test_helper"
require "webrick"

class GooseHydraBaseTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::GooseHydra::VERSION
  end

  TEST_SERVER_PORT = 3228

  def self.run_test_server
    WEBrick::HTTPServer.new(:Port => TEST_SERVER_PORT, :DocumentRoot => "#{Dir.pwd}/test/server").start
  end

  SETUP =
    begin
      test_server_thread = Thread.new do
        run_test_server
      end
      puts "\nRunning test server at http://localhost:#{TEST_SERVER_PORT}\n"
      
      Minitest.after_run do
        test_server_thread.exit
        test_server_thread = nil
      end
    end
end
