require "base"
require "pp"

class GooseHydraTest < GooseHydraBaseTest
  def test_that_it_has_a_version_number
    refute_nil ::GooseHydra::VERSION
  end

  def run_file(f)
    GooseHydra::Scout.execute(file: "#{Dir.pwd}/test/data/#{f}.yml") do |data, status|
      yield data, status
    end
  end

  def test_books
    run_file("books") do |data, status|
      assert status == :finished
      expected = YAML.load(File.read "#{Dir.pwd}/test/data/result.yml")
      if expected != data
        puts "\n\nBooks test - unexpected data, got:"
        pp data
        puts "\n\nExpected:"
        pp expected
        assert false
      else
        assert true
      end
    end
  end

  def test_timeout
    run_file("timeout") do |data, status|
      assert status == :timeout
    end
  end

  def test_wait
    run_file("wait") do |data, status|
      assert status == :finished
      assert data["content"] == "waited"
    end
  end

  def test_exceptions
    run_file("exceptions") do |data, status|
      if status != :finished
        puts "Unexpected status #{status}"
        assert false
      end

      assert data["stale_element"] == {"div" => "Redirecting to index...", "smth" => nil}
    end
  end
end

