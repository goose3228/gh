require_relative 'lib/goose_hydra/version'

Gem::Specification.new do |spec|
  spec.name          = "goose_hydra"
  spec.version       = GooseHydra::VERSION
  spec.authors       = ["Goose"]
  spec.email         = ["goose3228@protonmail.com"]

  spec.summary       = "Data extraction thing"
  spec.description   = "Extracting structured data from html by css selectors"
  spec.homepage      = "https://gitlab.com/goose3228/gh"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

  spec.metadata["homepage_uri"] = "https://gitlab.com/goose3228/gh"
  spec.metadata["source_code_uri"] = "https://gitlab.com/goose3228/gh"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "webdrivers", "~> 5.0"
  spec.add_dependency "selenium-webdriver", "~> 4.0"
end
