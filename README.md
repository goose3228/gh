# GooseHydra

```
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░▄▀▀▀▄░░░░░░░░░░░░░░░░░░
░░▄███▀░◐░▄▀▀▀▄░░░░░░░░░░░░░░░
░░░░▄███▀░◐░░░░▌░░░░░░░░░░░░░░
░░░░░▐░▄▀▀▀▄░░░▌░░░░░░░░░░░░░░
░░▄███▀░◐░░░▌░░▌░░░░░░░░░░░░░░
░░░░░░▌░░░░░▐▄▄▌░░░░░░░░░░░░░░
░░░░░░▌░░░░▄▀▒▒▀▀▀▀▄░░░░░░░░░░
░░░░░▐░░░░▐▒▒▒▒▒▒▒▒▀▀▄░░░░░░░░
░░░░░▐░░░░▐▄▒▒▒▒▒▒▒▒▒▒▀▄░░░░░░
░░░░░░▀▄░░░░▀▄▒▒▒▒▒▒▒▒▒▒▀▄░░░░
░░░░░░░░▀▄▄▄▄▄█▄▄▄▄▄▄▄▄▄▄▄▀▄░░
░░░░░░░░░░░░░▌▌░▌▌░░░░░░░░░░░░
░░░░░░░░░░░░░▌▌░▌▌░░░░░░░░░░░░
░░░░░░░░░░░▄▄▌▌▄▌▌░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
```

## Website parser

A simple ruby gem for parsing websites using firefox + Selenium to handle frontend rendering. The process is driven by yml configs, but at least minimum ruby code is required to get results.

While this tool is not suitable for data mining and other long-running or fully automated tasks, it works somewhat fine for manual extractions.

## Example

```yml
actions:
  - visit: https://example.com
  - extract: articles
  - browse: a
    hosts: example.com
    after:
      - extract: articles

pieces:
  articles:
    kind: hash
    multiple: true
    selector: .article-container
    children:
      - name: content
        kind: "html"
        selector: .article-content
      - name: title
        kind: "string"
        selector: h4
```

```rb
GooseHydra::Scout.execute(file: "articles.yml") do |data, status|
  puts "Finished, status is #{status}"
  File.write("result.json", JSON.generate(data))
end
```

This will parse articles with content and title from all pages, and save result in `result.json`.

## YAML Reference

### Pieces

List of data pieces to extract. Can have the following attributes:

- **selector** - css selector
- **kind** - data type: hash, string, or html
- **kind:** referrer - current browser referrer
- **kind:** url - current URL
- **kind:** index - HTML node index (`this.parentElement.children.indexOf(this)`)
- **multiple:** true - mark piece as array, usually true for root piece.
- **regexp:** true - further limit css selector with regexp, which should match content
- **attribute** - extract HTML attribute instead of content
- **children** - child pieces for hash kind. Their selectors will be run against parent node, not document root.
- **name** - piece name (note that root pieces have name as hash key, but children are array🐦)

### Actions

List of commands to execute.

- **visit** - visit a given URL
- **extract** - extract a piece
- **browse** - recursively browse links by that selector
- **hosts** - sub-option of browse, limits browsing to a specific hostname(s)
- **after** - another actions list to be executed after performing current action.
- **click**, **hover** - css selector, perform respective mouse action
- **times** - repeat multiple times
- **wait** - explicitly wait a few seconds
- **switch** - switch to an iframe, empty string to switch back

### Other options

- **timeout** - interrupt process after x seconds
- **wait** - explicit selenium wait, may be necessary for lazy loaded content

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
