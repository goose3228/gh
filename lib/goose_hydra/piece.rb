module GooseHydra
  class Piece
    KINDS = [:hash, :string, :html, :referrer, :url, :index]

    def initialize(kind: nil, selector: nil, name: nil, multiple: false, regexp: nil, attribute: nil, children: nil, browser: nil, wait: nil)
      @kind = kind ? kind.to_sym : :string
      raise "Unknown kind #{k}" unless KINDS.find(@kind)

      @name = name
      @multiple = multiple ? true : false
      @selector = selector.to_s
      @regexp = regexp && Regexp.new(regexp)
      @children = []
      @browser = browser
      @wait = wait && Selenium::WebDriver::Wait.new(:timeout => wait)

      @attribute = attribute

      children&.each do |h|
        c = self.class.from(h, browser)
        self.add_child c
      end
    end

    attr_reader :multiple
    attr_reader :selector
    attr_reader :regexp
    attr_reader :kind
    attr_reader :attribute
    attr_reader :name

    def add_child(child)
      raise "Can't add child to non-hash piece" if kind != :hash
      @children.push child
    end

    def extract(from = nil, to = nil)
      from ||= @browser.driver
      to ||= Hash.new

      if(@selector&.length > 0)
        nodes = @browser.find_elements(from, @selector, regexp: @regexp, wait: @wait)

        if @multiple
          to[@name] ||= []
          to[@name] += nodes.map { |node| self.send("extract_#{self.kind}", node) }
        else
          if nodes.length > 0
            to[@name] = self.send("extract_#{self.kind}", nodes.first)
          else
            to[@name] = nil
          end
        end
      else
        m = "extract_#{self.kind}"
        if self.method(m).arity == 0
          to[@name] = self.send(m)
        else
          to[@name] = self.send(m, from)
        end
      end
      to
    end

    def self.from(smth, browser)
      params = Helpers.mk_hash(smth, [:kind, :selector, :name, :multiple, :regexp, :attribute, :children, :wait]) unless params.class.name == "Hash"
      params[:browser] = browser
      self.new(**params)
    end

    def self.load(smth, browser)
      h = Hash.new
      Helpers.mk_hash(smth, [:pieces])[:pieces].each do |name, piece|
        piece["name"] ||= name
        h[name.to_sym] = self.from(piece, browser)
      end
      h
    end

    private

    def extract_hash(from)
      h = Hash.new
      @children.each do |c|
        c.extract from, h
      end
      h
    end

    def extract_string(from)
      begin
        if @attribute
          from.attribute @attribute
        else
          from.text
        end
      rescue *Browser::IGNORE_ERRORS
      end
    end

    def extract_html(from)
      @browser.driver.execute_script("return arguments[0].outerHTML;", from)
    end

    def extract_referrer
      @browser.referrer
    end

    def extract_url
      @browser.url
    end

    def extract_index(from)
      @browser.driver.execute_script("return Array.from(arguments[0].parentElement.children).indexOf(arguments[0]);", from)
    end
  end
end
