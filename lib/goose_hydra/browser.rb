require 'selenium-webdriver'
require 'webdrivers'

module GooseHydra
  class Browser
    def initialize(wait: nil, images: true, javascript: true, head: nil, profile: nil, network_timeout: nil)
      options = Selenium::WebDriver::Firefox::Options.new

      options.headless! unless head
      options.add_preference("permissions.default.image", 2) if images == false
      options.add_preference("javascript.enabled", false) if javascript == false
      options.add_preference("browser.download.dir", "/tmp")
      options.add_preference("browser.download.folderList", 2)
      options.add_preference("browser.download.useDownloadDir", true)

      options.profile = Selenium::WebDriver::Firefox::Profile.from_name(profile) if profile

      begin
        @driver = Selenium::WebDriver.for :firefox, options: options
      rescue Selenium::WebDriver::Error::WebDriverError
        self.class.set_driver_path()
        @driver = Selenium::WebDriver.for :firefox, options: options
      end

      @wait = wait && wait > 0 && Selenium::WebDriver::Wait.new(:timeout => wait)

      @url = nil
      @referrer = nil
      @driver.manage.timeouts.page_load = network_timeout || 20
      @driver.manage.timeouts.script = network_timeout || 20
      @current_window = @driver.window_handle
    end

    attr_reader :driver
    attr_reader :wait
    attr_reader :url
    attr_reader :referrer

    POSSIBLE_PATHS = ["/usr/bin/firefox", "/usr/bin/firefox-nightly"]
    def self.set_driver_path(path = nil)
      path = POSSIBLE_PATHS.find { |p| File.exist? p } unless path
      Selenium::WebDriver::Firefox.path = path
    end

    def self.load(smth)
      params = Helpers.mk_hash(smth, [:wait, :images, :javascript, :head, :profile, :network_timeout])
      self.new(**params)
    end

    def visit(url)
      @referrer = @url
      @url = url
      puts "Visiting #{url}"
      @driver.get url
    end

    def reset
      puts "Resetting browser..."
      @driver.switch_to.window @current_window
      puts "Switched to current window..."
      new_window = @driver.manage.new_window(:tab)
      @driver.close
      @driver.switch_to.window new_window
      @current_window = @driver.window_handle
      puts "Done"
    end

    def in_tab(url, &block)
      original_window = @driver.window_handle

      @driver.switch_to.window @driver.manage.new_window(:tab)
      visit url
      yield

      @driver.close
      @driver.switch_to.window original_window
    end

    IGNORE_ERRORS = [Selenium::WebDriver::Error::StaleElementReferenceError, Selenium::WebDriver::Error::TimeoutError]

    def find_elements(context, selector, min_length: 1, regexp: nil, wait: nil)
      nodes = []
      wait ||= @wait

      begin
        if wait
          wait.until do
            nodes = find_elements_nw(context, selector, regexp: regexp)
            nodes.length >= min_length
          end
        else
          nodes = find_elements_nw(context, selector, regexp: regexp)
        end
        nodes
      rescue *IGNORE_ERRORS
        []
      end
    end

    private
    def find_elements_nw(context, selector, regexp: nil)
      context.find_elements(css: selector).filter do |n|
        regexp ? n.text[regexp] : true
      end
    end
  end
end
