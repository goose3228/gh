module GooseHydra
  class Scout
    DEFAULT_RUN_TIMEOUT = 100

    def initialize(smth)
      @data = Hash.new
      self.load smth
    end

    attr_reader :data
    attr_reader :browser

    def piece(name)
      @pieces[name.to_sym] || raise("GH - Can't find piece #{name}!")
    end

    def load(something)
      @browser = Browser.load(something)
      @actions = Action.load(something, self)
      @pieces = Piece.load(something, @browser)
    end

    def quit
      puts "Exiting scout..."
      @browser.driver.quit
    end

    def self.execute(something = nil, file: nil)
      scout = nil
      something ||= File.read(file)
      timeout = Helpers.mk_hash(something, [:timeout])[:timeout] || DEFAULT_RUN_TIMEOUT

      status = Helpers.race(timeout) do
        scout = self.new(something)
        scout.run
      end

      scout&.quit

      yield scout&.data, status if block_given?
    end

    def run
      @actions.each do |a|
        a.exec
      end
    end
  end
end
