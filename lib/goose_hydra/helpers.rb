require "yaml"

module GooseHydra
  class Helpers
    def self.mk_hash(smth, whitelist = nil)
      if smth.class == String
        begin
          hash = JSON.parse smth
        rescue
          hash = YAML.load smth
        rescue
        end
      else
        hash = smth
      end

      return hash unless whitelist

      params = Hash.new
      whitelist.each do |p|
        params[p] = hash[p] || hash[p.to_s]
      end

      params
    end

    # Execute given block for no longer than <timeout> sec
    def self.race(timeout)
      status = :running

      main = Thread.new do
        begin
          yield
          status = :finished
        rescue => e
          status = :error
          pp e
          pp e.backtrace
        end
      end

      timeout.times do |i|
        break if status != :running
        sleep 1
      end

      if status == :running
        main.exit
        status = :timeout
      end

      status
    end
  end
end
