module GooseHydra
  class Action
    def initialize(click: nil, visit: nil, extract: nil, after: nil, scout: nil, browse: nil, hosts: nil, times: 1, wait: nil, hover: nil, switch: nil)
      @click = click
      @visit = visit
      @extract = extract&.to_sym
      @scout = scout
      @times = times || 1
      @wait = wait
      @hover = hover
      @switch = switch

      @browse = browse
      @hosts = hosts && !hosts.respond_to?(:find) ? [hosts] : hosts

      @after = []
      if after
        @after = self.class.load_plain(after, @scout)
      end
    end

    def self.from(smth, scout)
      params = Helpers.mk_hash(smth, [:click, :visit, :extract, :after, :browse, :hosts, :times, :wait, :hover, :switch])
      params[:scout] = scout
      self.new(**params)
    end

    def self.load(smth, scout)
      self.load_plain(Helpers.mk_hash(smth, [:actions])[:actions], scout)
    end

    def exec
      (1..@times).each do
        exec_nb
        exec_browse if @browse
      end
    end

    private

    def exec_nb
      exec_switch if @switch
      exec_hover if @hover
      exec_visit if @visit
      exec_click if @click
      exec_wait if @wait
      exec_extract if @extract
      exec_after if @after
    end

    def self.load_plain(arr, scout)
      actions = []
      arr.each do |a|
        actions.push self.from(a, scout)
      end
      actions
    end

    def exec_switch
      if @switch == ""
        puts "Switch to default content"
        @scout.browser.driver.switch_to.default_content
      else
        frame = @scout.browser.find_elements(@scout.browser.driver, @switch)&.first
        puts "Switch to iframe #{@switch} #{frame}"
        @scout.browser.driver.switch_to.frame frame if frame
      end
    end

    def exec_hover
      n = @scout.browser.find_elements(@scout.browser.driver, @hover)&.first
      if n
        @scout.browser.driver.execute_script("arguments[0].scrollIntoView({ behavior: 'instant', block: 'center', inline: 'center' })", n)
        sleep 0.5
        @scout.browser.driver.action.move_to(n).perform
      end
    end

    def exec_click
      n = @scout.browser.find_elements(@scout.browser.driver, @click)&.first
      @scout.browser.driver.execute_script("arguments[0].click()", n) if n
    end

    def exec_wait
      sleep @wait
    end

    def exec_visit
      @scout.browser.visit @visit
    end

    def exec_extract
      @scout.piece(@extract).extract(@scout.browser.driver, @scout.data)
    end

    def exec_after
      @after.each { |a| a.exec }
    end

    def exec_browse
      find_links
      while @urls_i < @urls.length
        begin
          @scout.browser.visit @urls[@urls_i]
        rescue
          puts "Error visiting, retry after 10 sec!"
          sleep 10
          begin
            @scout.browser.visit @urls[@urls_i]
          rescue
            puts "Retry failed, ignoring"
            return
          end
        end

        @urls_i += 1
        exec_nb
        find_links
      end
    end

    def find_links
      @urls ||= []
      @urls_i ||= 0

      @scout.browser.find_elements(@scout.browser.driver, @browse).each do |a|
        add_link a.attribute("href")
      end
    end

    def add_link(url)
      return unless url
      u = URI(url)
      u.fragment = nil
      return unless u.scheme == "http" || u.scheme == "https"
      return if @hosts && !@hosts.find { |h| h == u.host }
      return if @urls.find { |e| e == u.to_s }
      @urls.push(u.to_s)
    end
  end
end
