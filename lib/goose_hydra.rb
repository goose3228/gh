require "goose_hydra/version.rb"
require "goose_hydra/browser.rb"
require "goose_hydra/piece.rb"
require "goose_hydra/scout.rb"
require "goose_hydra/helpers.rb"
require "goose_hydra/action.rb"

module GooseHydra
  class Error < StandardError; end
end
